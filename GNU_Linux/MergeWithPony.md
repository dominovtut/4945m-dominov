<b>Step 1:</b> Add remote rep
```
git remote add upstream gogs@gogs.ponyorm.com:pony/pony.git
```

<b>Step 2:</b> fetch remote
```
git fetch upstream
```

<b>Step 3:</b> Merge your branch with pony/migrations_dev
```
git merge upstream/migrations_dev
```